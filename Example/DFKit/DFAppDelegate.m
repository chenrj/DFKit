//
//  DFAppDelegate.m
//  DFKit
//
//  Created by chenrj90 on 04/08/2017.
//  Copyright (c) 2017 chenrj90. All rights reserved.
//

#import "DFAppDelegate.h"
#import <DFKit/DFKitShare.h>
#import <DFKit/DFKitAnalyze.h>

@implementation DFAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [DFShareManager registerWithAppKey:@"5861e5daf5ade41326001eab"];
    [DFShareManager setAppKey:@"wxdc1e388c3822c80b" appSecretKey:@"3baf1193c85774b3fd9d18447d76cab0" platform:DFSharePlatformTypeWechatSession redirectURL:nil];
    [DFShareManager setAppKey:@"1105821097" appSecretKey:nil platform:DFSharePlatformTypeQQ redirectURL:nil];
    
    [DFShareManager setAppKey:@"3921700954" appSecretKey:@"04b48b094faeb16683c32669824ebdad" platform:DFSharePlatformTypeSinaWeibo redirectURL:nil];
    
    [DFAnalyzeManager startAnalyzePlatform:DFAnalyzePlatformUM withAppKey:@"54448737fd98c53d2b0018af" secretKey:@"secretstringaldfkals" channel:@"AppStore"];
    
    return YES;
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options {
    BOOL result = [DFShareManager handleOpenURL:url options:options];
    if (!result) {

    }
    return result;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    BOOL result = [DFShareManager handleOpenURL:url sourceApplication:sourceApplication annotation:sourceApplication];
    if (!result) {
        
    }
    return result;
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    BOOL result = [DFShareManager handleOpenURL:url];
    if (!result) {

    }
    return result;
}

@end
