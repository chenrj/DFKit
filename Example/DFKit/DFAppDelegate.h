//
//  DFAppDelegate.h
//  DFKit
//
//  Created by chenrj90 on 04/08/2017.
//  Copyright (c) 2017 chenrj90. All rights reserved.
//

@import UIKit;

@interface DFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
