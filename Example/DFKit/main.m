//
//  main.m
//  DFKit
//
//  Created by chenrj90 on 04/08/2017.
//  Copyright (c) 2017 chenrj90. All rights reserved.
//

@import UIKit;
#import "DFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DFAppDelegate class]));
    }
}
