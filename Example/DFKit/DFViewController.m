//
//  DFViewController.m
//  DFKit
//
//  Created by chenrj90 on 04/08/2017.
//  Copyright (c) 2017 chenrj90. All rights reserved.
//

#import "DFViewController.h"
#import <DFKit/DFKit.h>
#import <DFKit/DFKitShare.h>
#import <DFKit/DFKitAnalyze.h>

@interface DFViewController ()

@end

@implementation DFViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIView *view = DFMakeViewWithBackgroundColor([UIColor df_randomColor]);
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.mas_topLayoutGuideBottom);
    }];
    [view df_setIntrinsicContentSizeBlock:^CGSize(UIView *view) {
        return CGSizeMake(150, 150);
    }];
    [view addTapGestureRecognizer:^(UITapGestureRecognizer *recognizer, NSString *gestureId) {
//        [DFShareManager shareWebPage:@"http://www.baidu.com"
//                               title:@"度娘"
//                                desc:@"度娘懂你"
//                          thumbImage:@"https://mobile.umeng.com/images/pic/home/social/img-1.png"
//                        platformType:DFSharePlatformTypeWechatSession
//                          completion:^(id result, NSError *error) {
//                              NSLog(@"完成:%@ %@", result, error);
//                          }];
        
        [DFShareManager shareImage:@"https://ss0.bdstatic.com/94oJfD_bAAcT8t7mm9GUKT-xh_/timg?image&quality=100&size=b4000_4000&sec=1491811536&di=5396a7b0fa4f22cd4deec7f575f5b032&src=http://img3.duitang.com/uploads/item/201607/31/20160731173132_ua8yd.thumb.224_0.jpeg"
                             title:@"title"
                              desc:@"desc"
                        thumbImage:@"https://mobile.umeng.com/images/pic/home/social/img-1.png"
                      platformType:DFSharePlatformTypeWechatSession
                        completion:^(id result, NSError *error) {
                            NSLog(@"完成:%@ %@", result, error);
                        }];
    }];
    [view df_expandTouchAreaByInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    
    UILabel *label = DFMakeLabel(@"Hello DFRZW", [UIColor df_randomColor], [UIFont systemFontOfSize:16]);
    [self.view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(view.mas_bottom);
        make.centerX.equalTo(self.view);
    }];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [DFAnalyzeManager beginLogPageWithName:@"Main"];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [DFAnalyzeManager endLogPageWithName:@"Main"];
}

@end
