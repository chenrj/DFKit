//
//  UIView+DFExtension.h
//  Pods
//
//  Created by rujia chen on 2017/04/02.
//
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, DFLineDirection) {
    DFLineDirectionHorizontal,
    DFLineDirectionVertical
};

typedef CGSize(^DFIntrinsicContentSizeBlock)(UIView *view);

@interface UIView (DFExtension)

/**
 获取所在的ViewController

 @return UIViewController 可为空
 */
- (UIViewController *)df_viewController;

/**
 通过代码块来替换原生的intrinscContentSize方法

 @param block
 */
- (void)df_setIntrinsicContentSizeBlock:(DFIntrinsicContentSizeBlock)block;


/**
 扩大UIView的点击区域

 @param insets 扩散大小
 */
- (void)df_expandTouchAreaByInsets:(UIEdgeInsets)insets;


/**
 相当于原生的intrinsicContentSize, 因为内部做过替换

 @return
 */
- (CGSize)df_intrinsicContentSize;


/**
 生成一条一像素的线

 @param lineDirection 方向

 @return
 */
+ (instancetype)lineWithDirection:(DFLineDirection)lineDirection;


/**
 生成一条一像素的线

 @param lineDirection 方向
 @param color         颜色

 @return 
 */
+ (instancetype)lineWithDirection:(DFLineDirection)lineDirection color:(UIColor *)color;

@end
