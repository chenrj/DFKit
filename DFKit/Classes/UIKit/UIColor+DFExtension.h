//
//  UIColor+DFExtension.h
//  Pods
//
//  Created by rujia chen on 2017/04/04.
//
//

#import <UIKit/UIKit.h>

@interface UIColor (DFExtension)


/**
 随机颜色

 @return UIColor
 */
+ (UIColor *)df_randomColor;


/**
 十六进制rgb颜色

 @param hexString

 @return UIColor
 */
+ (UIColor *)df_colorByHex:(NSString *)hexString;

@end
