//
//  UIColor+DFExtension.m
//  Pods
//
//  Created by rujia chen on 2017/04/04.
//
//

#import "UIColor+DFExtension.h"

@implementation UIColor (DFExtension)

+ (UIColor *)df_colorByHex:(NSString *)hexString {
    unsigned int redInt_, greenInt_, blueInt_;
    NSRange rangeNSRange_;
    rangeNSRange_.length = 2;
    rangeNSRange_.location = 0;
    [[NSScanner scannerWithString:[hexString substringWithRange:rangeNSRange_]] scanHexInt:&redInt_];

    rangeNSRange_.location = 2;
    [[NSScanner scannerWithString:[hexString substringWithRange:rangeNSRange_]] scanHexInt:&greenInt_];
    
    rangeNSRange_.location = 4;
    [[NSScanner scannerWithString:[hexString substringWithRange:rangeNSRange_]] scanHexInt:&blueInt_];
    
    return [UIColor colorWithRed:(float)(redInt_/255.0f) green:(float)(greenInt_/255.0f) blue:(float)(blueInt_/255.0f) alpha:1.0f];
}

+ (UIColor *)df_randomColor {
    CGFloat hue = (arc4random() % 256 / 256.0);
    CGFloat saturation = (arc4random() % 128 / 256.0) + 0.5;
    CGFloat brightness = (arc4random() % 128 / 256.0) + 0.5;
    UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
    
    return color;
}

@end
