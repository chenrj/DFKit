//
//  DFKitMarco.h
//  Pods
//
//  Created by rujia chen on 17/4/8.
//
//

#ifndef DFKitMarco_h
#define DFKitMarco_h

#define DF_ONE_PIXEL (1/[UIScreen mainScreen].scale)

#define DF_SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)

#define DF_SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)

#define DF_SCREEN_SCALE ([UIScreen mainScreen].scale)

NS_INLINE UIView *DFMakeViewWithBackgroundColor(UIColor *backgroundColor) {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = backgroundColor;
    return view;
}

NS_INLINE UILabel *DFMakeLabel(NSString *text, UIColor *textColor, UIFont *font) {
    UILabel *label = [[UILabel alloc] init];
    label.text = text;
    label.textColor = textColor;
    label.font = font;
    return label;
}

#endif /* DFKitMarco_h */
