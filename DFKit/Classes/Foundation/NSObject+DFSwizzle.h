//
//  NSObject+DFSwizzle.h
//  Pods
//
//  Created by rujia chen on 2017/04/02.
//
//

#import <Foundation/Foundation.h>

@interface NSObject (DFSwizzle)


/**
 方法交换

 @param origSel_
 @param altSel_
 @param error_

 @return
 */
+ (BOOL)df_swizzleMethod:(SEL)origSel_ withMethod:(SEL)altSel_ error:(NSError**)error_;


/**
 类方法交换

 @param origSel_
 @param altSel_
 @param error_

 @return 
 */
+ (BOOL)df_swizzleClassMethod:(SEL)origSel_ withClassMethod:(SEL)altSel_ error:(NSError**)error_;

@end
