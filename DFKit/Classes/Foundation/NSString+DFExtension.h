//
//  NSString+DFExtension.h
//  Pods
//
//  Created by rujia chen on 2017/04/04.
//
//

#import <Foundation/Foundation.h>

#define DF_CONCAT_STRING(str1,str2) ([NSString stringWithFormat:@"%@%@", (str1), (str2)])

@interface NSString (DFExtension)


/**
 MD5加密字符串

 @return
 */
- (NSString *)df_stringUsingMD5;

@end
