//
//  DFShareManager.m
//  Pods
//
//  Created by rujia chen on 17/4/8.
//
//

#import "DFShareManager.h"
#import <UMSocialCore/UMSocialCore.h>

@implementation DFShareManager

+ (BOOL)handleOpenURL:(NSURL *)url {
    return [[UMSocialManager defaultManager] handleOpenURL:url];
}

+ (BOOL)handleOpenURL:(NSURL *)url options:(NSDictionary*)options {
    return [[UMSocialManager defaultManager] handleOpenURL:url options:options];
}

+ (BOOL)handleOpenURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[UMSocialManager defaultManager] handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
}

+ (void)registerWithAppKey:(NSString *)appkey {
    [[UMSocialManager defaultManager] setUmSocialAppkey:appkey];
}

+ (BOOL)setAppKey:(NSString *)appKey
     appSecretKey:(NSString *)appSecretKey
         platform:(DFSharePlatformType)platform
      redirectURL:(NSString *)redirectURL {
    UMSocialPlatformType umSocialType = [self getUMTypeFrom:platform];

    return [[UMSocialManager defaultManager] setPlaform:umSocialType appKey:appKey appSecret:appSecretKey redirectURL:redirectURL];
}

+ (void)shareWebPage:(NSString *)urlString
               title:(NSString *)title
                desc:(NSString *)desc
          thumbImage:(id)thumbImage
        platformType:(DFSharePlatformType)platform
          completion:(DFShareCompletionBlock)completion{
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:title descr:desc thumImage:thumbImage];
    shareObject.webpageUrl = urlString;
    [self shareObject:shareObject toPlatForm:platform completion:completion];
}

+ (void)shareImage:(id)shareImage
               title:(NSString *)title
                desc:(NSString *)desc
          thumbImage:(id)thumbImage
      platformType:(DFSharePlatformType)platform
        completion:(DFShareCompletionBlock)completion{
    UMShareImageObject *shareObject = [UMShareImageObject shareObjectWithTitle:title descr:desc thumImage:thumbImage];
    shareObject.shareImage = shareImage;
    
    [self shareObject:shareObject toPlatForm:platform completion:completion];
}

#pragma mark - private

+ (void)shareObject:(UMShareObject *)shareObject toPlatForm:(DFSharePlatformType)platform completion:(DFShareCompletionBlock)completion {
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    messageObject.shareObject = shareObject;
    [[UMSocialManager defaultManager] shareToPlatform:[self getUMTypeFrom:platform] messageObject:messageObject currentViewController:nil completion:completion];
}

+ (UMSocialPlatformType)getUMTypeFrom:(DFSharePlatformType)platform {
    UMSocialPlatformType umSocialType = UMSocialPlatformType_UnKnown;
    switch (platform) {
        case DFSharePlatformTypeWechatSession:
            umSocialType = UMSocialPlatformType_WechatSession;
            break;
        case DFSharePlatformTypeWechatTimeline:
            umSocialType = UMSocialPlatformType_WechatTimeLine;
            break;
        case DFSharePlatformTypeQQ:
            umSocialType = UMSocialPlatformType_QQ;
            break;
        case DFSharePlatformTypeQzone:
            umSocialType = UMSocialPlatformType_Qzone;
            break;
        case DFSharePlatformTypeSinaWeibo:
            umSocialType = UMSocialPlatformType_Sina;
            break;
        default:
            break;
    }
    
    return umSocialType;
}

@end
