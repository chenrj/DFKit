//
//  DFShareManager.h
//  Pods
//
//  Created by rujia chen on 17/4/8.
//
//

#import <Foundation/Foundation.h>


/**
 分享平台

 - DFSharePlatformTypeWechatSession:  微信会话
 - DFSharePlatformTypeWechatTimeline: 微信朋友圈
 - DFSharePlatformTypeQQ:             QQ会话
 - DFSharePlatformTypeQzone:          QQ空间
 - DFSharePlatformTypeSinaWeibo:      新浪微博
 */
typedef NS_ENUM(NSUInteger, DFSharePlatformType) {
    DFSharePlatformTypeWechatSession,
    DFSharePlatformTypeWechatTimeline,
    DFSharePlatformTypeQQ,
    DFSharePlatformTypeQzone,
    DFSharePlatformTypeSinaWeibo
};


/**
 分享完成回调代码块

 @param result result
 @param error  错误
 */
typedef void(^DFShareCompletionBlock)(id result, NSError *error);

@interface DFShareManager : NSObject


/**
 从其他app回到本app的回调

 @param url

 @return
 */
+ (BOOL)handleOpenURL:(NSURL *)url;


/**
 从其他app回到本app的回调

 @param url
 @param options

 @return
 */
+ (BOOL)handleOpenURL:(NSURL *)url options:(NSDictionary*)options;

/**
 从其他app回到本app的回调

 @param url
 @param sourceApplication
 @param annotation

 @return
 */
+ (BOOL)handleOpenURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;

/**
 注册统计工具key

 @param appkey appkey
 */
+ (void)registerWithAppKey:(NSString *)appkey;

/**
 注册分享平台

 @param appKey       第三方平台的appKey（QQ平台为appID）
 @param appSecretKey 第三方平台的appSecret（QQ平台为appKey）
 @param platform     分享平台 @see DFSharePlatformType
 @param redirectURL  redirectURL
 */
+ (BOOL)setAppKey:(NSString *)appKey
     appSecretKey:(NSString *)appSecretKey
         platform:(DFSharePlatformType)platform
      redirectURL:(NSString *)redirectURL;

/**
 分享网页

 @param urlString  网页地址
 @param title      标题
 @param desc       描述
 @param thumbImage 缩略图 类型可以为UIImage、NSData、NSString
 @param platform   分享平台 @see DFSharePlatformType
 @param completion 完成回调
 */
+ (void)shareWebPage:(NSString *)urlString
               title:(NSString *)title
                desc:(NSString *)desc
          thumbImage:(id)thumbImage
        platformType:(DFSharePlatformType)platform
          completion:(DFShareCompletionBlock)completion;

/**
 分享图片
 
 @param shareImage 要分享的图片 类型可以为UIImage、NSData、NSString
 @param title      标题
 @param desc       详情
 @param thumbImage 缩略图 类型可以为UIImage、NSData、NSString
 @param platform   分享平台 @see DFSharePlatformType
 @param completion 完成回调
 */
+ (void)shareImage:(id)shareImage
             title:(NSString *)title
              desc:(NSString *)desc
        thumbImage:(id)thumbImage
      platformType:(DFSharePlatformType)platform
        completion:(DFShareCompletionBlock)completion;

@end
