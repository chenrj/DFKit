//
//  DFAnalyzeManager.h
//  Pods
//
//  Created by rujia chen on 17/4/11.
//
//

#import <Foundation/Foundation.h>


/**
 统计平台

 - DFAnalyzePlatformUM:          友盟
 - DFAnalyzePlatformTalkingData: 灵动分析
 */
typedef NS_ENUM(NSUInteger, DFAnalyzePlatform) {
    DFAnalyzePlatformUM,
    DFAnalyzePlatformTalkingData
};

@interface DFAnalyzeManager : NSObject


/**
 统计开始方法

 @param platform  平台 @see DFAnalyzePlatform
 @param appkey    appkey
 @param secretKey secret key (灵动分析不需要)
 @param channel   渠道名
 */
+ (void)startAnalyzePlatform:(DFAnalyzePlatform)platform withAppKey:(NSString *)appkey secretKey:(NSString *)secretKey channel:(NSString *)channel;


/**
 记录页面

 @param name 名称
 */
+ (void)beginLogPageWithName:(NSString *)name;


/**
 停止记录

 @param name 名称
 */
+ (void)endLogPageWithName:(NSString *)name;


/**
 记录事件

 @param eventId 事件名
 */
+ (void)logEvent:(NSString *)eventId;

/**
 记录事件

 @param eventId 事件名
 @param label   标签名
 */
+ (void)logEvent:(NSString *)eventId label:(NSString *)label;


/**
 记录事件

 @param eventId 事件名
 @param params  参数
 */
+ (void)logEvent:(NSString *)eventId parameters:(NSDictionary *)params;

@end
