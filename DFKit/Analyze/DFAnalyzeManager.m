//
//  DFAnalyzeManager.m
//  Pods
//
//  Created by rujia chen on 17/4/11.
//
//

#import "DFAnalyzeManager.h"
#import <UMMobClick/MobClick.h>
#import "TalkingData.h"

static BOOL DFAnalyzeStartFlagUM = NO;
static BOOL DFAnalyzeStartFlaTD = NO;
@implementation DFAnalyzeManager

+ (void)startAnalyzePlatform:(DFAnalyzePlatform)platform withAppKey:(NSString *)appkey secretKey:(NSString *)secretKey channel:(NSString *)channel{
    switch (platform) {
        case DFAnalyzePlatformUM:
        {
            UMAnalyticsConfig *config = [UMAnalyticsConfig sharedInstance];
            config.appKey = appkey;
            config.secret = secretKey;
            config.channelId = channel;
            [MobClick setCrashReportEnabled:YES];
            [MobClick startWithConfigure:config];
            DFAnalyzeStartFlagUM = YES;
        }
            break;
        case DFAnalyzePlatformTalkingData:
        {
            [TalkingData setExceptionReportEnabled:YES];
            [TalkingData sessionStarted:appkey withChannelId:channel];
            DFAnalyzeStartFlaTD = YES;
            break;
        }
        default:
        {
            NSLog(@"统计平台未知!");
        }
            break;
    }
}

+ (void)beginLogPageWithName:(NSString *)name {
    if (DFAnalyzeStartFlagUM) {
        [MobClick beginLogPageView:name];
    }
    if (DFAnalyzeStartFlaTD) {
        [TalkingData trackPageBegin:name];
    }
}

+ (void)endLogPageWithName:(NSString *)name {
    if (DFAnalyzeStartFlagUM) {
        [MobClick endLogPageView:name];
    }
    if (DFAnalyzeStartFlaTD) {
        [TalkingData trackPageEnd:name];
    }
}

+ (void)logEvent:(NSString *)eventId {
    if (DFAnalyzeStartFlagUM) {
        [MobClick event:eventId];
    }
    if (DFAnalyzeStartFlaTD) {
        [TalkingData trackEvent:eventId];
    }
}

+ (void)logEvent:(NSString *)eventId label:(NSString *)label {
    if (DFAnalyzeStartFlagUM) {
        [MobClick event:eventId label:label];
    }
    if (DFAnalyzeStartFlaTD) {
        [TalkingData trackEvent:eventId label:label];
    }
}

+ (void)logEvent:(NSString *)eventId parameters:(NSDictionary *)params {
    if (DFAnalyzeStartFlagUM) {
        [MobClick event:eventId attributes:params];
    }
    if (DFAnalyzeStartFlaTD) {
        [TalkingData trackEvent:eventId label:nil parameters:params];
    }
}

@end
