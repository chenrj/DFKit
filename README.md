# DFKit

[![CI Status](http://img.shields.io/travis/chenrj90/DFKit.svg?style=flat)](https://travis-ci.org/chenrj90/DFKit)
[![Version](https://img.shields.io/cocoapods/v/DFKit.svg?style=flat)](http://cocoapods.org/pods/DFKit)
[![License](https://img.shields.io/cocoapods/l/DFKit.svg?style=flat)](http://cocoapods.org/pods/DFKit)
[![Platform](https://img.shields.io/cocoapods/p/DFKit.svg?style=flat)](http://cocoapods.org/pods/DFKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

DFKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "DFKit"
```

```
<key>LSApplicationQueriesSchemes</key>
<array>
<string>linkedin</string>
<string>linkedin-sdk2</string>
<string>linkedin-sdk</string>
<string>wechat</string>
<string>weixin</string>
<string>sinaweibohd</string>
<string>sinaweibo</string>
<string>sinaweibosso</string>
<string>weibosdk</string>
<string>weibosdk2.5</string>
<string>mqqapi</string>
<string>mqq</string>
<string>mqqOpensdkSSoLogin</string>
<string>mqqconnect</string>
<string>mqqopensdkdataline</string>
<string>mqqopensdkgrouptribeshare</string>
<string>mqqopensdkfriend</string>
<string>mqqopensdkapi</string>
<string>mqqopensdkapiV2</string>
<string>mqqopensdkapiV3</string>
<string>mqqopensdkapiV4</string>
<string>mqzoneopensdk</string>
<string>wtloginmqq</string>
<string>wtloginmqq2</string>
<string>mqqwpa</string>
<string>mqzone</string>
<string>mqzonev2</string>
<string>mqzoneshare</string>
<string>wtloginqzone</string>
<string>mqzonewx</string>
<string>mqzoneopensdkapiV2</string>
<string>mqzoneopensdkapi19</string>
<string>mqzoneopensdkapi</string>
<string>mqqbrowser</string>
<string>mttbrowser</string>
<string>TencentWeibo</string>
<string>tencentweiboSdkv2</string>
<string>alipay</string>
<string>alipayshare</string>
<string>renrenios</string>
<string>renrenapi</string>
<string>renren</string>
<string>renreniphone</string>
<string>laiwangsso</string>
<string>yixin</string>
<string>yixinopenapi</string>
<string>yixinoauth</string>
<string>yixinfav</string>
<string>laiwangsso</string>
<string>instagram</string>
<string>whatsapp</string>
<string>line</string>
<string>tumblr</string>
<string>fbapi</string>
<string>fb-messenger-api</string>
<string>fbauth2</string>
<string>fbshareextension</string>
<string>kakao6d5e4ca98ba3b944020c2c90953318e9</string>
<string>kakaokompassauth</string>
<string>storykompassauth</string>
<string>kakaolink</string>
<string>kakaotalk-4.5.0</string>
<string>kakaostory-2.9.0</string>
<string>pinterestsdk.v1</string>
<string>dingtalk</string>
<string>dingtalk-open</string>
<string>evernote</string>
<string>en</string>
<string>enx</string>
<string>evernotecid</string>
<string>evernotemsg</string>
<string>youdaonote</string>
<string>ynotedictfav</string>
<string>com.youdao.note.todayViewNote</string>
<string>ynotesharesdk</string>
<string>gplus</string>
<string>pocket</string>
<string>readitlater</string>
<string>pocket-oauth-v1</string>
<string>fb131450656879143</string>
<string>en-readitlater-5776</string>
<string>com.ideashower.ReadItLaterPro3</string>
<string>com.ideashower.ReadItLaterPro</string>
<string>com.ideashower.ReadItLaterProAlpha</string>
<string>com.ideashower.ReadItLaterProEnterprise</string>
<string>vk</string>
<string>vk-share</string>
<string>vkauthorize</string>
<string>fbauth</string>
<string>fbauth2</string>
<string>fb-messenger-api20140430</string>
<string>fb-messenger-platform</string>
<string>fb-messenger-platform-20150128</string>
<string>fb-messenger-platform-20150218</string>
<string>fb-messenger-platform-20150305</string>
<string>fb-messenger-share</string>
</array>
```

## Author

chenrj90, chenrj90@163.com

## License

DFKit is available under the MIT license. See the LICENSE file for more info.
